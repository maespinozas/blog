require 'test_helper'

#class PostTest < ActiveSupport::TestCase
  # test "the truth" do
  #  assert true
  # end
#end

#adding code from Modue 7
class PostTest < ActiveSupport::TestCase
test "invalid_with_empty_attributes" do
post = Post.new # create an empty post model
assert post.invalid? # An empty Post model should be invalid
assert post.errors[:title].any? # The title field should have validation errors
assert post.errors[:body].any? # The body field should have validation errors
end
test "requires_body" do
post = Post.create(:body => "This is an awesome post.")
assert post.invalid?
assert post.errors[:title].any?
end
test "requires_title" do
post = Post.create(:title => "My Great Post")
assert post.invalid?
assert post.errors[:body].any?
end
test "valid_with_title_and_body" do
post = Post.create(:title => "My Great Post", :body => "This is an awesome post.")
assert post.valid?
end
end

